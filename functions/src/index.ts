import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as htmlToJson from 'html-to-json';

const nodemailer = require("nodemailer");

admin.initializeApp();

const gmailEmail = "sri.propiedades@gmail.com"; //functions.config().gmail.email;
const gmailPassword = "l0nd0nUPW1F1"; //functions.config().gmail.password;

const APP_NAME = "Showroom Inmobiliario";

const mailTransport = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: gmailEmail,
    pass: gmailPassword
  }
});

import * as cors from "cors";
const corsOptions: cors.CorsOptions = {
  origin: true
};

const corsHandler = cors(corsOptions);

// Funcion para registrar propiedad
export const registroPropiedad = functions.https.onRequest(
  async (request, response) => {

    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Credentials', 'true'); // vital

    corsHandler(request, response, async () => {

        

      // Obtengo los valores
      let cod = request.body.codigo; // Codigo propiedad
      let imgHeader = request.body.imgHeader; // Img cabecera
      let titulo = request.body.titulo; // Titulo de la propiedad
      let ubicacion = request.body.ubicacion; // Ubicación
      let precio = request.body.precio; // Precio propiedad
      let area = request.body.area; // Area
      let habitaciones = request.body.habitaciones; // Habitaciones
      let banos = request.body.banos; // Baños
      let parqueaderos = request.body.parqueaderos; // Parqueaderos
      let administracion = request.body.administracion; // Administración
      let predial = request.body.predial; // Predial
      let areas = request.body.areas; // Areas
      let caracteristicas = request.body.caracteristicas; // Caracteristicas
      let imagenes = request.body.imagenes; // Imagenes
      let imgAsesor = request.body.imgAsesor; // Imagenes asesor
      let nombreAsesor = request.body.nombreAsesor; // Nombre asesor
      let fraseAsesor = request.body.fraseAsesor; // Frase asesor
      let telefonoAsesor = request.body.telefonoAsesor; // Telefono asesor
      let correoAsesor = request.body.correoAsesor; // Correo asesor
      let correoenviar = request.body.email;
      let asunto = request.body.asunto;

      guardarDatosPropiedad(request);

      let areasUl = "<ul>";

      areas.forEach(area => {
        areasUl += `<li style="list-style-type: disc;">${area}</li>`;
      });

      areasUl += `</ul>`;

      let html = `

        <!DOCTYPE html>
        <html lang="es">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>SRI | Propiedad</title>
        
            <style>

    * {
        font-family: Verdana !important;
    }

    .mat-elevation-z9 {
        box-shadow: none;
    }

    .mat-elevation-z5 {
        box-shadow: none;
    }

    .contenedor {
        background: #fff;
        margin: 0 5rem;
        border: 1px solid #dcdcdc;
    }

    .imagen-cabecera {
        display: inline-grid;
        height: 75vh;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        width: 100%;
        -webkit-print-color-adjust: exact;
    }

    .precio {
        right: 0;
        background: #FF5500;
        padding: 5px 5px;
        font-size: 25px !important;
        color: #fff;
        font-weight: 300;
        font-size: 1.5em;
        width: 60% !important;
        border-top-right-radius: 20px;
        border-color: transparent;
    }

    .codigo {
        background: rgba(105, 104, 103, .6);
        padding: 10px 5px;
        font-size: 17px !important;
        color: #fff;
        font-weight: 300;
        font-size: 1.5em;
        width: 40% !important;
        text-align: left;
        padding-left: 1em;
    }

    .datos-relevantes {
        display: flex;
    }

    .detalle-propiedad {
        display: flex;
        flex-wrap: nowrap;
        background-color: #fff;
        padding-top: 1em;
    }

    .detalle-propiedad>div {
        height: 100%;
        width: 40%;
        margin: 0;
        text-align: center;
        font-size: 30px;
    }

    .imagenes {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        width: 60% !important;
    }

    .imagenes>img {
        width: 47%;
        height: 170px;
        flex: 1 1 45%;
        border-radius: 5px;
    }

    .imagenes>img:nth-child(odd) {
        margin: 5px 5px 5px 10px;
    }

    .imagenes>img:nth-child(even) {
        margin: 5px 10px 5px 5px;
    }

    .informacion {
        top: -1.6em;
    }

    .iconos-infomacion {
        left: 0;
        width: 100%;
        text-align: center;
        display: inline-table;
    }

    .info-general {
        border-bottom: 1px solid #dedede;
    }

    .info-general-icon {
        background: #fff;
        color: #394446;
        height: 80px;
        width: 22%;
        display: inline-table;
        border-right: 1px solid #e2dcdc;
        margin: 5px;
        padding-top: 10px;
    }

    .info-general-icon:last-child {
        border-right: 0;
    }

    .info-general-icon>p {
        margin: 0;
        font-size: 18px;
    }

    .info-general>div {
        flex: 1 1 25%;
        line-height: initial;
    }

    .titulo {
        background: #394447;
        border: 2px solid #394447;
        color: #fff;
        font-family: Roboto;
        font-weight: 500;
        padding: 16px 20px;
        text-align: left;
        font-size: 17px;
    }

    .ubicacion {
        background: #BCC86E;
        border: 2px solid #BCC86E;
        color: #394447;
        font-family: Roboto;
        font-weight: 500;
        padding: 16px 20px;
        text-align: left;
        font-size: 17px;
    }

    .titulo-area {
        background: #394447;
        color: #fff;
        font-family: Roboto;
        font-weight: 500;
        padding: 12px 20px;
        text-align: left;
        font-size: 14px;
    }

    .detalle-area>ul {
        border-right: 1px solid #dedede;
        margin: 0 0px;
        padding: 20px 30px;
        line-height: 1.6em;
        columns: 2;
        -webkit-columns: 2;
        -moz-columns: 2;
        list-style-type: none;
        list-style-position: inside;
        font-size: 14px;
        text-align: left;
    }

    .caracteristicas-area>p {
        border-right: 1px solid #dedede;
        margin: 0 0px;
        padding: 20px 30px;
        line-height: 1.6em;
        list-style-type: none;
        list-style-position: inside;
        font-size: 14px;
        text-align: left;
    }

    .pie-pagina {
        padding-top: 3em;
        padding-bottom: 2em;
        font-family: "Roboto";
        vertical-align: middle;
        align-items: center;
        display: flex;
    }

    .pie-pagina>* {
        width: 33%;
    }

    .container-foto-asesor {
        margin-left: 1em;
        display: inline-flex;
        justify-items: center;
        justify-content: center;
    }

    .foto-asesor {
        width: 10em;
        height: 10em;
        border-radius: 10em;
        margin: 0 auto;
    }

    .container-detalle-asesor {
        margin-right: 1em;
    }

    .container-detalle-asesor>img {
        width: 14px;
    }

    .nombre-asesor {
        font-size: 1.6em;
        font-weight: 500;
        color: #394446;
    }

    .nombre-asesor>img {
        padding: 3px 6px;
        vertical-align: bottom;
        margin-right: 4px;
    }

    .frase-asesor {
        color: #99a931;
        font-weight: 500;
        margin: 0;
    }

    .frase-asesor>img {
        padding: 3px 6px;
        vertical-align: bottom;
        margin-right: 4px;
    }

    .telefono-asesor {
        color: #fa5503;
        font-weight: 700;
        font-size: 1.1em;
        margin: 0;
    }

    .telefono-asesor>img {
        padding: 3px 6px;
        vertical-align: bottom;
        margin-right: 4px;
    }

    .correo-asesor {
        color: #99a931;
        font-weight: 500;
        margin: 0;
        font-size: 12px;
        text-decoration: none;
    }

    .correo-asesor>img {
        padding: 3px 6px;
        vertical-align: bottom;
        margin-right: 4px;
    }

    .direccion {
        color: #99a931;
        font-weight: 500;
        margin: 0;
    }

    .direccion>img {
        padding: 3px 6px;
        vertical-align: bottom;
        margin-right: 4px;
    }

    .telefono {
        color: #fa5503;
        font-weight: 700;
        font-size: 1.1em;
        margin: 0;
    }

    .telefono>img {
        padding: 3px 6px;
        vertical-align: bottom;
        margin-right: 4px;
    }

    .container-contacto>img {
        width: 14px;
    }

    .container-contacto>.direccion {
        color: #99a931;
        font-weight: 500;
        margin: 0;
    }

    .container-contacto>.direccion>img {
        padding: 3px 6px;
        vertical-align: bottom;
        margin-right: 4px;
    }

    .container-contacto>.telefono {
        color: #fa5503;
        font-weight: 700;
        font-size: 1.1em;
        margin: 0;
    }

    .container-contacto>.telefono>img {
        padding: 3px 6px;
        vertical-align: bottom;
        margin-right: 4px;
    }

    .container-contacto>.web {
        color: #99a931;
        font-weight: 500;
        margin: 0;
        text-decoration: none;
    }

    .container-contacto>.web>img {
        padding: 3px 6px;
        vertical-align: bottom;
        margin-right: 4px;
    }

    .container-logo-lonja {
        background: #bcc86e;
        height: 80px;
        display: inline-flex;
        align-items: center;
        justify-content: right;
        justify-items: right;
        border-top-left-radius: 70px;
        width: 100%;
        box-shadow: 0 4px 8px rgba(0, 0, 0, .3);
    }

    .container-logo-lonja>img {
        padding-top: 0;
        margin: 0 auto;
        width: 190px !important;
        height: auto !important;
    }

    .detalle-valores {
        display: inline-flex;
        width: 100%;
        border-bottom: 1px solid #fff;
    }

    .admin-area {
        width: 50%;
        background: #394447;
        color: #fff;
        font-family: Roboto;
        font-weight: 500;
        padding: 12px 20px;
        text-align: left;
        font-size: 14px;
        border-right: 1px solid #fff;
        text-align: center;
    }

    .predial-area {
        width: 50%;
        background: #394447;
        color: #fff;
        font-family: Roboto;
        font-weight: 500;
        padding: 12px 20px;
        text-align: left;
        font-size: 14px;
        text-align: center;
    }

    .predial-area-blanco {
        width: 50%;
        background: #fff;
        color: #394447;
        font-family: Roboto;
        font-weight: 500;
        padding: 12px 20px;
        text-align: left;
        font-size: 14px;
        text-align: center;
    }

    .admin-area-blanco {
        width: 50%;
        background: #fff;
        color: #394447;
        font-family: Roboto;
        font-weight: 500;
        padding: 12px 20px;
        text-align: left;
        font-size: 14px;
        border-right: 1px solid #fff;
        text-align: center;
    }


    @media print {
        .imagen-cabecera {
            height: 45vh !important;
        }
    }

    @page {
        margin: 0;
    }

    @media screen and (max-width:900px) {

        .contenedor {
            margin: 0;
        }

        .frase-asesor>img,
        .telefono-asesor>img,
        .correo-asesor>img,
        .container-contacto>.direccion>img,
        .container-contacto>.telefono>img,
        .nombre-asesor>img {
            display: none;
        }

        .nombre-asesor {
            font-size: 1.4em;
        }

        .frase-asesor {
            font-size: 1.2em;
        }

        .correo-asesor {
            font-size: 1.1em;
        }

        .detalle-propiedad {
            display: initial;
        }

        .detalle-propiedad>div {
            width: 100%;
        }

        .precio {
            border-radius: 0;
        }

        .pie-pagina {
            display: block;
            text-align: center;
            margin-top: 2em;
        }

        .pie-pagina>* {
            width: 100%;
        }

        .container-logo-lonja {
            height: 100px;
            border-top-left-radius: 0;
            margin-top: 2em;
        }

        .container-logo-lonja>img {
            padding-top: 0em;
        }

        
        .imagenes {
            width: 100% !important;
        }

        .info-general-icon {
            text-align: center;
            width: 20%;
        }

        .detalle-valores {
            display: inline-flex;
            width: 100%;
            border-bottom: 1px solid #fff;
        }

        .admin-area {
            width: 50%;
            background: #394447;
            color: #fff;
            font-family: Roboto;
            font-weight: 500;
            padding: 12px 20px;
            text-align: left;
            font-size: 14px;
            border-right: 1px solid #fff;
            text-align: center;
        }

        .predial-area {
            width: 50%;
            background: #394447;
            color: #fff;
            font-family: Roboto;
            font-weight: 500;
            padding: 12px 20px;
            text-align: left;
            font-size: 14px;
            text-align: center;
        }

        .predial-area-blanco {
            width: 50%;
            background: #fff;
            color: #394447;
            font-family: Roboto;
            font-weight: 500;
            padding: 12px 20px;
            text-align: left;
            font-size: 14px;
            text-align: center;
        }

        .admin-area-blanco {
            width: 50%;
            background: #fff;
            color: #394447;
            font-family: Roboto;
            font-weight: 500;
            padding: 12px 20px;
            text-align: left;
            font-size: 14px;
            border-right: 1px solid #fff;
            text-align: center;
        }

        .titulo-area {
            text-align: center !important;
        }

        .caracteristicas-area>p {
            text-align: center !important;
        }
    }

    @media (min-width: 320px) and (max-width: 480px) {

        .imagen-cabecera {
            height: 40vh !important;
        }

        .logo-sri>img {
            width: 55% !important;
        }

        .imagenes>img {
            width: 100% !important;
            height: 100% !important;
            margin: 0 !important;
            border-radius: 0 !important;
        }

        .mat-elevation-z5 {
            box-shadow: none !important;
        }

        .nombre-asesor {
            font-size: 1.5em !important;
        }

        .container-foto-asesor {
            margin-left: 0 !important;
        }

        .foto-asesor {
            width: 16em !important;
            height: 16em !important;
            border-radius: 16em !important;
        }

        .container-logo-lonja {
            height: 66px !important;
        }

        .container-logo-lonja>img {
            padding-top: 0 !important;
        }

        .detalle-valores {
            display: inline-flex;
            width: 100%;
            border-bottom: 1px solid #fff;
        }

        .admin-area {
            width: 50%;
            background: #394447;
            color: #fff;
            font-family: Roboto;
            font-weight: 500;
            padding: 12px 20px;
            text-align: left;
            font-size: 14px;
            border-right: 1px solid #fff;
            text-align: center;
        }

        .predial-area {
            width: 50%;
            background: #394447;
            color: #fff;
            font-family: Roboto;
            font-weight: 500;
            padding: 12px 20px;
            text-align: left;
            font-size: 14px;
            text-align: center;
        }

        .predial-area-blanco {
            width: 50%;
            background: #fff;
            color: #394447;
            font-family: Roboto;
            font-weight: 500;
            padding: 12px 20px;
            text-align: left;
            font-size: 14px;
            text-align: center;
        }

        .admin-area-blanco {
            width: 50%;
            background: #fff;
            color: #394447;
            font-family: Roboto;
            font-weight: 500;
            padding: 12px 20px;
            text-align: left;
            font-size: 14px;
            border-right: 1px solid #fff;
            text-align: center;
        }

    }

        
            </style>
        </head>
        <body>
            <div class="contenedor mat-elevation-z9" style="border: 1px solid #dcdcdc;">
                <div class="imagen-cabecera" style="display: inline-grid;
                background-image: url('${imgHeader}');
                height: 75vh;
                background-size: cover;
                background-repeat: no-repeat;
                background-position: center;
                width: 100%;
                position: relative;">
                    <span class="logo-sri">
                        <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/logo-sri.png?alt=media&token=80313f53-f763-4c02-badb-fb534236aa73">
                    </span>
                </div>
                <div class="detalle-propiedad">
                    <div class="informacion" style="border: 1px solid #dedede;border-top-right-radius: 22px;">
                        <div class="info-general mat-elevation-z5">
                            <div class="datos-relevantes">
                                <div class="codigo">
                                    Cod. ${cod}
                                </div>
            
                                <div class="precio">
                                    ${precio}
                                </div>
                            </div>
                            <div class="titulo">
                                    ${titulo}
                            </div>
                            <div class="ubicacion">
                                <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-ubicacion.png?alt=media&token=65334b33-8f62-46e4-a855-2f17ace4dabe" style="padding-right:10px;">${ubicacion}
                            </div>
                            <div class="info-detallada">
                                <div class="iconos-infomacion">
                                    <div class="info-general-icon">
                                        <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-casa.png?alt=media&token=1caf3cdf-8475-4e65-95f8-4b45f7581c48">
                                        <p>${area}</p>
                                    </div>
                                    <div class="info-general-icon">
                                        <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-cama.png?alt=media&token=334f9f80-b696-42ca-8d90-44eb5c08b9c9">
                                        <p>${habitaciones}</p>
                                    </div>
                                    <div class="info-general-icon">
                                        <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-sanitario.png?alt=media&token=695be873-93b3-4a9c-b77b-3e398d3f1440">
                                        <p>${banos}</p>
                                    </div>
                                    <div class="info-general-icon" style="border: 0 !important;">
                                        <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-carro.png?alt=media&token=db90e58c-597a-43f9-83ef-b8f680d3cd17">
                                        <p>${parqueaderos}</p>
                                    </div>
                                </div>

                                <div class="detalle-valores">
                                    <div class="admin-area">
                                        Admin
                                    </div>
                                    <div class="predial-area">
                                        Predial
                                    </div>
                                </div>

                                <div class="detalle-valores">
                                    <div class="admin-area-blanco">
                                        ${administracion}
                                    </div>
                                    <div class="predial-area-blanco">
                                        ${predial}
                                    </div>
                                </div>

                                <div class="detalle-area">
                                    <div class="titulo-area">
                                        Extras
                                    </div>
                                    ${areasUl}
                                </div>
                                <div class="caracteristicas-area">
                                    <div class="titulo-area">
                                        Características
                                    </div>
                                    <p>${caracteristicas}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="imagenes" style="display: block !important;">
                        <img class="mat-elevation-z5" src="${imagenes[0]}">
                        <img class="mat-elevation-z5" src="${imagenes[1]}">
                        <img class="mat-elevation-z5" src="${imagenes[2]}">
                        <img class="mat-elevation-z5" src="${imagenes[3]}">
                        <img class="mat-elevation-z5" src="${imagenes[4]}">
                        <img class="mat-elevation-z5" src="${imagenes[5]}">
                    </div>
                </div>
                <div class="pie-pagina" fxLayout="row" fxFlexFill>
                    <div class="container-foto-asesor" fxFlex="33" fxFlex="33">
                        <img src="${imgAsesor}" class="foto-asesor">
                    </div>
                    <div fxFlex="33">
                        <div class="container-detalle-asesor">
                            <p class="nombre-asesor">
                                ${nombreAsesor}
                            </p>
                            <p class="frase-asesor">
                                <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-pencil.png?alt=media&token=27542bd1-741b-4615-b064-32f755f47ce7"> "Agente Inmobiliario"
                            </p>
                            <p class="telefono-asesor">
                                <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-phone.png?alt=media&token=52460fb8-aec1-4524-9897-22503b3507b2"> ${telefonoAsesor}
                            </p>
                            <a class="correo-asesor" href="mailto:${correoAsesor}?Subject=Hola" target="_top">
                                <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-email.png?alt=media&token=5617be76-5cb2-4a03-8ea3-e1fa1fb15612"> ${correoAsesor}
                            </a>
                        </div>
                    </div>
                    <div fxFlex="33">
                        <div class="container-contacto">
                            <p>
                                <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-blank.png?alt=media&token=905a070c-e033-4fbb-bd97-0f3264e2cf51">
                            </p>
                            <p class="direccion">
                                <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-pin.png?alt=media&token=cde0f529-0c9e-4424-ba97-dc4ee5718e1e"> Mall Indiana local 171
                            </p>
                            <p class="telefono">
                                <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-phone.png?alt=media&token=52460fb8-aec1-4524-9897-22503b3507b2"> (4) 444 42 52
                            </p>
                            <a class="web" href="https://www.SRinmobiliario.com" target="_blank">
                                www.SRinmobiliario.com
                            </a>
                        </div>
                    </div>
                </div>
                <div class="container-logo-lonja" fxFlex="100">
                    <img  style="width: 115px; height: fit-content; " src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/logo-lalonja.png?alt=media&token=d6d5010b-f3a0-4430-8e89-ab1e2868499d">
                </div>
            </div>
        </body>
        </html>`;


        htmlToJson.parse(html).then((respJSON:any)=>{
            if (correoenviar && asunto) {
                enviarCorreo(correoenviar, asunto, html);
                setTimeout(() => {
                    response.status(200).send(respJSON);
                }, 3000);
            } else {
                response.status(200).send(respJSON);
            }
        })


    });
  }
);

// Consulta de propiedad
export const propiedad = functions.https.onRequest(async (request, response) => {

    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Credentials', 'true'); // vital

    corsHandler(request, response, async () => {

        let fsdb = admin.firestore();
        let codigo = request.param("codigo");
      
        // Consulto el registro
        fsdb
          .collection("propiedades")
          .doc(`${codigo}`)
          .get()
          .then(function(doc) {
            if (doc.exists) {
              let info = doc.data();
              imprimir(info, response);
            } else {
              response.send(`No se encontro propiedad ${codigo}`);
            }
          })
          .catch(function(error) {
            console.log("Error getting document:", error);
          });

    })
});

// Imprimir pagina por codigo
function imprimir(info, response: any) {
  // Obtengo los valores
  let cod = info.codigo; // Codigo propiedad
  let imgHeader = info.imgHeader; // Img cabecera
  let titulo = info.titulo; // Titulo de la propiedad
  let ubicacion = info.ubicacion; // Ubicación
  let precio = info.precio; // Precio propiedad
  let area = info.area; // Area
  let habitaciones = info.habitaciones; // Habitaciones
  let banos = info.banos; // Baños
  let parqueaderos = info.parqueaderos; // Parqueaderos
  let administracion = info.administracion; // Administración
  let predial = info.predial; // Predial
  let areas = info.areas; // Areas
  let caracteristicas = info.caracteristicas; // Caracteristicas
  let imagenes = info.imagenes; // Imagenes
  let imgAsesor = info.imgAsesor; // Imagenes asesor
  let nombreAsesor = info.nombreAsesor; // Nombre asesor
  let fraseAsesor = info.fraseAsesor; // Frase asesor
  let telefonoAsesor = info.telefonoAsesor; // Telefono asesor
  let correoAsesor = info.correoAsesor; // Correo asesor
  let correoenviar = info.email;
  let asunto = info.asunto;

  let areasUl = "<ul>";

  areas.forEach(area => {
    areasUl += `<li style="list-style-type: disc;">${area}</li>`;
  });

  areasUl += `</ul>`;

  let html = `
        <!DOCTYPE html>
        <html lang="es">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>SRI | MLS: # ${cod} - ${titulo} en ${ubicacion}, Precio $ ${precio}, area: ${area}, ${habitaciones} habitaciones, ${parqueaderos} parqueaderos, ${banos} baños, ${caracteristicas} </title>
            <meta property="og:image" content="${imgHeader}">
            <meta name="description" content="SRI | MLS: # ${cod} - ${titulo} en ${ubicacion}, Precio $ ${precio}, area: ${area}, ${habitaciones} habitaciones, ${parqueaderos} parqueaderos, ${banos} baños, ${caracteristicas}">
            <div id='hidden' style='display:none;'><img src="${imgHeader}"></div>


            <style>

            * {
                font-family: Verdana !important;
            }

                        .detalle-valores {
                display: inline-flex;
                width: 100%;
                border-bottom: 1px solid #fff;
            }

            .admin-area {
                width: 50%;
                background: #394447;
                color: #fff;
                font-family: Roboto;
                font-weight: 500;
                padding: 12px 20px;
                text-align: left;
                font-size: 14px;
                border-right: 1px solid #fff;
                text-align: center;
            }

            .predial-area {
                width: 50%;
                background: #394447;
                color: #fff;
                font-family: Roboto;
                font-weight: 500;
                padding: 12px 20px;
                text-align: left;
                font-size: 14px;
                text-align: center;
            }

            .predial-area-blanco {
                width: 50%;
                background: #fff;
                color: #394447;
                font-family: Roboto;
                font-weight: 500;
                padding: 12px 20px;
                text-align: left;
                font-size: 14px;
                text-align: center;
            }

            .admin-area-blanco {
                width: 50%;
                background: #fff;
                color: #394447;
                font-family: Roboto;
                font-weight: 500;
                padding: 12px 20px;
                text-align: left;
                font-size: 14px;
                border-right: 1px solid #fff;
                text-align: center;
            }

        .mat-elevation-z9 {
            box-shadow: none;
            //box-shadow: 0 5px 6px -3px rgba(0, 0, 0, .2), 0 9px 12px 1px rgba(0, 0, 0, .14), 0 3px 16px 2px rgba(0, 0, 0, .12);
        }
    
        .mat-elevation-z5 {
            box-shadow: none;
            //box-shadow: 0 3px 5px -1px rgba(0, 0, 0, .2), 0 5px 8px 0 rgba(0, 0, 0, .14), 0 1px 14px 0 rgba(0, 0, 0, .12);
        }
    
        .contenedor {
            background: #fff;
            margin: 0 5rem;
            border: 1px solid #dcdcdc;
        }
    
        .imagen-cabecera {
            display: inline-grid;
            height: 75vh;
            
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            width: 100%;
            -webkit-print-color-adjust: exact;
            
        }
    
        .precio {
            /*position: absolute;*/
            right: 0;
            background: #FF5500;
            padding: 5px 5px;
            font-size: 25px !important;
            color: #fff;
            font-weight: 300;
            font-size: 1.5em;
            width: 60% !important;
            border-top-right-radius: 20px;
            border-color: transparent;
        }
    
        .codigo {
            /*position: absolute;*/
            background: rgba(105, 104, 103,.6);
            padding: 10px 5px;
            font-size: 17px !important;
            color: #fff;
            font-weight: 300;
            font-size: 1.5em;
            width: 40% !important;
            text-align: left;
            padding-left: 1em;
    
        }
    
        .datos-relevantes {
            display: flex;
        }
    
        .detalle-propiedad {
            display: flex;
            flex-wrap: nowrap;
            background-color: #fff;
            padding-top: 1em;
        }
    
        .detalle-propiedad>div {
            height: 100%;
            width: 40%;
            margin: 0;
            text-align: center;
            font-size: 30px;
        }
    
        .imagenes {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            width:60% !important;
        }
    
        .imagenes>img {
            width: 47%;
            height: 220px;
            flex: 1 1 45%;
            border-radius: 5px;
        }
    
        .imagenes>img:nth-child(odd) {
            margin: 5px 5px 5px 10px;
        }
    
        .imagenes>img:nth-child(even) {
            margin: 5px 10px 5px 5px;
        }
    
        .informacion {
            
            top: -1.6em;
        }
    
        .info-detallada {
            
        }
    
        .iconos-infomacion {
            left: 0;
            width: 100%;
            text-align: center;
            display: inline-table;
            /*border-right: 1px solid #dedede;*/
        }
    
        .info-general {
            border-bottom: 1px solid #dedede;
        }
    
        .info-general-icon {
            background: #fff;
            color: #394446;
            /*border: 1px solid #e2dcdc;*/
            height: 80px;
            width: 22%;
            display: inline-table;
            border-right: 1px solid #e2dcdc;
            margin: 5px;
            padding-top: 10px;
            /*box-shadow: 0 5px 6px -3px rgba(0, 0, 0, .2), 0 9px 12px 1px rgba(0, 0, 0, .14), 0 3px 16px 2px rgba(0, 0, 0, .12);*/
        }
    
        .info-general-icon:last-child {
            border-right: 0;
        }
    
    
        .info-general-icon>p {
            margin: 0;
            font-size: 18px;
        }
    
        .info-general>div {
            flex: 1 1 25%;
            line-height: initial;
        }
    
        .titulo {
            background: #394447;
            border: 2px solid #394447;
            color: #fff;
            font-family: Roboto;
            font-weight: 500;
            padding: 16px 20px;
            text-align: left;
            font-size: 17px;
        }
    
        .ubicacion {
            background: #BCC86E;
            border: 2px solid #BCC86E;
            color: #394447;
            font-family: Roboto;
            font-weight: 500;
            padding: 16px 20px;
            text-align: left;
            font-size: 17px;
        }
    
        .titulo-area {
            background: #394447;
            color: #fff;
            font-family: Roboto;
            font-weight: 500;
            padding: 12px 20px;
            text-align: left;
            font-size: 14px;
        }
    
        .detalle-area>ul {
            border-right: 1px solid #dedede;
            margin: 0 0px;
            padding: 20px 30px;
            line-height: 1.6em;
            columns: 2;
            -webkit-columns: 2;
            -moz-columns: 2;
            list-style-type: none;
            list-style-position: inside;
            font-size: 14px;
            text-align: left;
        }
        
        .caracteristicas-area>p {
            border-right: 1px solid #dedede;
            margin: 0 0px;
            padding: 20px 30px;
            line-height: 1.6em;
            list-style-type: none;
            list-style-position: inside;
            font-size: 14px;
            text-align: left;
        }
    
        .pie-pagina {
            padding-top: 3em;
            padding-bottom: 2em;
            font-family: "Roboto";
            vertical-align: middle;
            align-items: center;
            display: flex;
        }
    
        .pie-pagina>* {
            width: 33%;
        }
    
        .container-foto-asesor {
            margin-left: 1em;
            display: inline-flex;
            justify-items: center;
            justify-content: center;
        }
    
        .foto-asesor {
            width: 10em;
            height: 10em;
            border-radius: 10em;
            margin: 0 auto;
        }
    
        .container-detalle-asesor {
            margin-right: 1em;
        }
    
        .container-detalle-asesor>img {
            width: 14px;
        }
    
        .nombre-asesor {
            font-size: 1.6em;
            font-weight: 500;
            color: #394446;
        }
    
        .nombre-asesor>img {
            padding: 3px 6px;
            vertical-align: bottom;
            margin-right: 4px;
        }
    
        .frase-asesor {
            color: #99a931;
            font-weight: 500;
            margin: 0;
        }
    
        .frase-asesor>img {
            padding: 3px 6px;
            vertical-align: bottom;
            margin-right: 4px;
        }
    
        .telefono-asesor {
            color: #fa5503;
            font-weight: 700;
            font-size: 1.1em;
            margin: 0;
        }
    
        .telefono-asesor>img {
            padding: 3px 6px;
            vertical-align: bottom;
            margin-right: 4px;
        }
    
        .correo-asesor {
            color: #99a931;
            font-weight: 500;
            margin: 0;
            font-size: 12px;
            text-decoration: none;
        }
    
        .correo-asesor>img {
            padding: 3px 6px;
            vertical-align: bottom;
            margin-right: 4px;
        }
    
        .direccion {
            color: #99a931;
            font-weight: 500;
            margin: 0;
        }
    
        .direccion>img {
            padding: 3px 6px;
            vertical-align: bottom;
            margin-right: 4px;
        }
    
        .telefono {
            color: #fa5503;
            font-weight: 700;
            font-size: 1.1em;
            margin: 0;
        }
    
        .telefono>img {
            padding: 3px 6px;
            vertical-align: bottom;
            margin-right: 4px;
        }
    
        .container-contacto>img {
            width: 14px;
        }
    
        .container-contacto>.direccion {
            color: #99a931;
            font-weight: 500;
            margin: 0;
        }
    
        .container-contacto>.direccion>img {
            padding: 3px 6px;
            vertical-align: bottom;
            margin-right: 4px;
        }
    
        .container-contacto>.telefono {
            color: #fa5503;
            font-weight: 700;
            font-size: 1.1em;
            margin: 0;
        }
    
        .container-contacto>.telefono>img {
            padding: 3px 6px;
            vertical-align: bottom;
            margin-right: 4px;
        }
    
        .container-contacto>.web {
            color: #99a931;
            font-weight: 500;
            margin: 0;
            text-decoration: none;
        }
    
        .container-contacto>.web>img {
            padding: 3px 6px;
            vertical-align: bottom;
            margin-right: 4px;
        }
    
        .container-logo-lonja {
            background: #bcc86e;
            height: 80px;
            display: inline-flex;
            align-items: center;
            justify-content: right;
            justify-items: right;
            border-top-left-radius: 70px;
            width: 100%;
            box-shadow: 0 4px 8px rgba(0, 0, 0, .3);
        }
    
        .container-logo-lonja>img {
            padding-top: 0;
            // padding-left: 1em;
            margin: 0 auto;
            width: 190px !important;
            height: auto !important;
        }

        @media print {
            .imagen-cabecera {
                height: 45vh !important;
            }
        }

        @page {
            margin: 0;
        }
    
        @media screen and (max-width:900px) {
            
            .contenedor {
                margin: 0;
            }
            .frase-asesor>img, .telefono-asesor>img, .correo-asesor>img, 
            .container-contacto>.direccion>img,.container-contacto>.telefono>img, .nombre-asesor>img {
                display: none;
            }

            .nombre-asesor {
                font-size: 1.4em;
            }
            .frase-asesor {
                font-size:1.2em;
            }
            .correo-asesor {
                font-size: 1.1em;
            }

            .detalle-propiedad {
                display: initial;
            }
            .detalle-propiedad>div {
                width: 100%;
            }
            .precio {
                border-radius: 0;
            }
            .pie-pagina {
                display: block;
                text-align: center;
                margin-top: 2em;
            }
            .foto-asesor {
                width: 7em;
                height: 7em;
                border-radius: 7em;
            }
            .pie-pagina>* {
                width: 100%;
            }
    
            .container-logo-lonja {
                height: 100px;
                border-top-left-radius: 0;
                margin-top: 2em;
            }
    
            .container-logo-lonja>img {
                padding-top: 0em;
            }
            .imagenes>img {
                width: 93%;
            }
            .imagenes {
                width: 100% !important;
            }
            .info-general-icon {
                text-align: center;
                width: 20%;
            }

            .detalle-valores {
                display: inline-flex;
                width: 100%;
                border-bottom: 1px solid #fff;
            }

            .admin-area {
                width: 50%;
                background: #394447;
                color: #fff;
                font-family: Roboto;
                font-weight: 500;
                padding: 12px 20px;
                text-align: left;
                font-size: 14px;
                border-right: 1px solid #fff;
                text-align: center;
            }

            .predial-area {
                width: 50%;
                background: #394447;
                color: #fff;
                font-family: Roboto;
                font-weight: 500;
                padding: 12px 20px;
                text-align: left;
                font-size: 14px;
                text-align: center;
            }

            .predial-area-blanco {
                width: 50%;
                background: #fff;
                color: #394447;
                font-family: Roboto;
                font-weight: 500;
                padding: 12px 20px;
                text-align: left;
                font-size: 14px;
                text-align: center;
            }

            .admin-area-blanco {
                width: 50%;
                background: #fff;
                color: #394447;
                font-family: Roboto;
                font-weight: 500;
                padding: 12px 20px;
                text-align: left;
                font-size: 14px;
                border-right: 1px solid #fff;
                text-align: center;
            }
            .titulo-area {
                text-align: center !important;
            }

            .caracteristicas-area>p {
                text-align: center !important;
            }

        }

        @media (min-width: 320px) and (max-width: 480px) {
  
            .imagen-cabecera {
                height: 40vh !important;                
            }

            .logo-sri > img > img {
                width: 55% !important;
            }

            .imagenes>img {
                width: 100% !important;
                height: 100% !important;
                margin: 0 !important;
                border-radius: 0 !important;
            }

            .mat-elevation-z5 {
                box-shadow: none !important;
            }

            .nombre-asesor {
                font-size: 1.5em !important;
            }

            .container-foto-asesor {
                margin-left: 0 !important;
            }

            .foto-asesor {
                width: 16em !important;
                height: 16em !important;
                border-radius: 16em !important;
            }

            .container-logo-lonja {
                height: 66px !important;
            }
            
            .container-logo-lonja > img {
                padding-top: 0!important;
            }

                        .detalle-valores {
                display: inline-flex;
                width: 100%;
                border-bottom: 1px solid #fff;
            }

            .admin-area {
                width: 50%;
                background: #394447;
                color: #fff;
                font-family: Roboto;
                font-weight: 500;
                padding: 12px 20px;
                text-align: left;
                font-size: 14px;
                border-right: 1px solid #fff;
                text-align: center;
            }

            .predial-area {
                width: 50%;
                background: #394447;
                color: #fff;
                font-family: Roboto;
                font-weight: 500;
                padding: 12px 20px;
                text-align: left;
                font-size: 14px;
                text-align: center;
            }

            .predial-area-blanco {
                width: 50%;
                background: #fff;
                color: #394447;
                font-family: Roboto;
                font-weight: 500;
                padding: 12px 20px;
                text-align: left;
                font-size: 14px;
                text-align: center;
            }

            .admin-area-blanco {
                width: 50%;
                background: #fff;
                color: #394447;
                font-family: Roboto;
                font-weight: 500;
                padding: 12px 20px;
                text-align: left;
                font-size: 14px;
                border-right: 1px solid #fff;
                text-align: center;
            }
        }
        
        </style>
        </head>
        <body onload="window.print()">
            <div class="contenedor mat-elevation-z9" style="border: 1px solid #dcdcdc;">
                <div class="imagen-cabecera" style="display: inline-grid;
                background-image: url('${imgHeader}');
                height: 75vh;
                background-size: cover;
                background-repeat: no-repeat;
                background-position: center;
                width: 100%;
                position: relative;">
                    <span class="logo-sri">
                        <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/logo-sri.png?alt=media&token=80313f53-f763-4c02-badb-fb534236aa73">
                    </span>
                </div>
                <div class="detalle-propiedad">
                    <div class="informacion" style="border: 1px solid #dedede;border-top-right-radius: 22px;">
                        <div class="info-general mat-elevation-z5">
                            <div class="datos-relevantes">
                                <div class="codigo">
                                    Cod.# ${cod}
                                </div>
            
                                <div class="precio">
                                    ${precio}
                                </div>
                            </div>
                            <div class="titulo">
                                    ${titulo}
                            </div>
                            <div class="ubicacion">
                                <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-ubicacion.png?alt=media&token=65334b33-8f62-46e4-a855-2f17ace4dabe" style="padding-right:10px;">${ubicacion}
                            </div>
                            <div class="info-detallada">
                                <div class="iconos-infomacion">
                                    <div class="info-general-icon">
                                        <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-casa.png?alt=media&token=1caf3cdf-8475-4e65-95f8-4b45f7581c48">
                                        <p>${area}</p>
                                    </div>
                                    <div class="info-general-icon">
                                        <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-cama.png?alt=media&token=334f9f80-b696-42ca-8d90-44eb5c08b9c9">
                                        <p>${habitaciones}</p>
                                    </div>
                                    <div class="info-general-icon">
                                        <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-sanitario.png?alt=media&token=695be873-93b3-4a9c-b77b-3e398d3f1440">
                                        <p>${banos}</p>
                                    </div>
                                    <div class="info-general-icon" style="border: 0 !important;">
                                        <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-carro.png?alt=media&token=db90e58c-597a-43f9-83ef-b8f680d3cd17">
                                        <p>${parqueaderos}</p>
                                    </div>
                                </div>
                                <div class="detalle-valores">
                                    <div class="admin-area">
                                        Admin
                                    </div>
                                    <div class="predial-area">
                                        Predial
                                    </div>
                                </div>
        
                                <div class="detalle-valores">
                                    <div class="admin-area-blanco">
                                        ${administracion}
                                    </div>
                                    <div class="predial-area-blanco">
                                        ${predial}
                                    </div>
                                </div>

                                <div class="detalle-area">
                                    <div class="titulo-area">
                                        Extras
                                    </div>
                                    ${areasUl}
                                </div>
                                <div class="caracteristicas-area">
                                    <div class="titulo-area">
                                        Características
                                    </div>
                                    <p>${caracteristicas}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="imagenes" style="display: block !important;">
                        <img class="mat-elevation-z5" src="${imagenes[0]}">
                        <img class="mat-elevation-z5" src="${imagenes[1]}">
                        <img class="mat-elevation-z5" src="${imagenes[2]}">
                        <img class="mat-elevation-z5" src="${imagenes[3]}">
                        <img class="mat-elevation-z5" src="${imagenes[4]}">
                        <img class="mat-elevation-z5" src="${imagenes[5]}">
                    </div>
                </div>
                <div class="pie-pagina" fxLayout="row" fxFlexFill>
                    <div class="container-foto-asesor" fxFlex="33" fxFlex="33">
                        <img src="${imgAsesor}" class="foto-asesor">
                    </div>
                    <div fxFlex="33">
                        <div class="container-detalle-asesor">
                            <p class="nombre-asesor">
                                ${nombreAsesor}
                            </p>
                            <p class="frase-asesor">
                                <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-pencil.png?alt=media&token=27542bd1-741b-4615-b064-32f755f47ce7"> "Agente Inmobiliario"
                            </p>
                            <p class="telefono-asesor">
                                <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-phone.png?alt=media&token=52460fb8-aec1-4524-9897-22503b3507b2"> ${telefonoAsesor}
                            </p>
                            <a class="correo-asesor" href="mailto:${correoAsesor}?Subject=Hola" target="_top">
                                <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-email.png?alt=media&token=5617be76-5cb2-4a03-8ea3-e1fa1fb15612"> ${correoAsesor}
                            </a>
                        </div>
                    </div>
                    <div fxFlex="33">
                        <div class="container-contacto">
                            <p>
                                <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-blank.png?alt=media&token=905a070c-e033-4fbb-bd97-0f3264e2cf51">
                            </p>
                            <p class="direccion">
                                <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-pin.png?alt=media&token=cde0f529-0c9e-4424-ba97-dc4ee5718e1e"> Mall Indiana local 171
                            </p>
                            <p class="telefono">
                                <img src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/icon-phone.png?alt=media&token=52460fb8-aec1-4524-9897-22503b3507b2"> (4) 444 42 52
                            </p>
                            <a class="web" href="https://www.SRinmobiliario.com" target="_blank">
                                www.SRinmobiliario.com
                            </a>
                        </div>
                    </div>
                </div>
                <div class="container-logo-lonja" fxFlex="100">
                    <img  style="width: 115px; height: fit-content;" src="https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/logo-lalonja.png?alt=media&token=d6d5010b-f3a0-4430-8e89-ab1e2868499d">
                </div>
            </div>
        </body>
        </html>`;

  response.status(200).send(html);
}

// Function enviar email
function enviarCorreo(email, asunto, mensaje) {
  const mailOptions = {
    from: `${APP_NAME} <noreply@srinmobiliario.com>`,
    to: email,
    subject: asunto,
    html: mensaje
  };

  return mailTransport.sendMail(mailOptions).then(() => {
    return console.log("Se envio el correo:", email);
  });
}

// Guardar datos de propiedad
function guardarDatosPropiedad(info: any) {
  let cod = info.body.codigo; // Codigo propiedad
  let fsdb = admin.firestore();
  let imgHeader = info.body.imgHeader; // Img cabecera
  let titulo = info.body.titulo; // Titulo de la propiedad
  let ubicacion = info.body.ubicacion; // Ubicación
  let precio = info.body.precio; // Precio propiedad
  let area = info.body.area; // Area
  let habitaciones = info.body.habitaciones; // Habitaciones
  let banos = info.body.banos; // Baños
  let parqueaderos = info.body.parqueaderos; // Parqueaderos
  let administracion = info.body.administracion; // Administración
  let predial = info.body.predial; // Predial
  let areas = info.body.areas; // Areas
  let caracteristicas = info.body.caracteristicas; // Caracteristicas
  let imagenes = info.body.imagenes; // Imagenes
  let imgAsesor = info.body.imgAsesor; // Imagenes asesor
  let nombreAsesor = info.body.nombreAsesor; // Nombre asesor
  let fraseAsesor = info.body.fraseAsesor; // Frase asesor
  let telefonoAsesor = info.body.telefonoAsesor; // Telefono asesor
  let correoAsesor = info.body.correoAsesor; // Correo asesor
  let correoenviar = info.body.email;
  let asunto = info.body.asunto;

  let item: any = {
    codigo: cod,
    imgHeader: imgHeader,
    titulo: titulo,
    ubicacion: ubicacion,
    precio: precio,
    area: area,
    habitaciones: habitaciones,
    banos: banos,
    parqueaderos: parqueaderos,
    administracion: administracion,
    predial: predial,
    areas: areas,
    caracteristicas: caracteristicas,
    imagenes: imagenes,
    imgAsesor: imgAsesor,
    nombreAsesor: nombreAsesor,
    fraseAsesor: fraseAsesor,
    telefonoAsesor: telefonoAsesor,
    correoAsesor: correoAsesor
  };

  // consulto si ya existe

  return fsdb
    .collection(`propiedades`)
    .doc(`${cod}`)
    .set(item);

  //return fsdb.collection(`propiedades`).add(item);
}
