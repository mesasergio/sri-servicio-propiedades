# Documentación tecnica Servicio SRI - 22 de Agosto de 2018
#### Autor: Sergio Mesa - Trusty Digital S.A.S

Esta documentación expone ejemplos de las opciones para utilizar el servicio de presentación de información de las propiedades de MLS/SRI

## Arquitectura & Tecnologia

1. El servicio esta expuesto en firebase cloud Functions en la url [sri-volante.firebaseapp.com](URL "https://sri-volante.firebaseapp.com/").

2. Se utilizo la libreria *nodemailer version 4.6.7* para el envio de los correos electrónico


## Servicio disponibles

### registroPropiedad

**URI del servicio POST:** [https://us-central1-sri-volante.cloudfunctions.net/registroPropiedad](URL "https://us-central1-sri-volante.cloudfunctions.net/registroPropiedad").

*Este servicio tiene varias funcionalidades a implementar:*

* Realiza el registro de una propiedad en la base de datos para consulta posterior e impresión llamando la función guardarDatosPropiedad().
* Realiza el envio de correo electrónico a un cuenta seleccionada por medio del atributo del body llamado **email**
* Realiza la impresión de la pagina devolviendo el html en el request con codigo 200.

**Atributos del servicio**

1. **codigo:** Codigo de la propiedad, este será la clave para almacenarlo en base de datos
2. **imgHeader:** Url de la imagen que se mostrará en el encabezado
3. **titulo:** Titulo de la propiedad, es el texto que se muestrá para describir la propiedad
4. **ubicacion:** Ubicación de la propiedad
5. **precio:** Precio de la propiedad
6. **area:** Tamaño de area de la propiedad
7. **habitaciones:** Número de habitaciones
8. **banos:** Número de baños de la propiedad
9. **parqueaderos:** Número de parqueaderos
10. **administracion:** Valor de la administración
11. **predial:** Valor del predial
12. **areas:** Array de areas de interes de la propiedad como Salón común, Piscina, etc..
13. **caracteristicas:** Texto de caracteristicas de la propiedad
14. **imagenes:** Array para las 6 imagenes de la propiedad
15. **imgAsesor:** Url de la foto del asesor que la ofrece
16. **nombreAsesor:** Nombre del asesor que la ofrece
17. **fraseAsesor:** Frase del asesor
18. **telefonoAsesor:** Número teléfonico del asesor
19. **correoAsesor:** Correo electrónico del asesor
20. **email:** Correo electrónico de envio del volante, si este **Campo esta lleno se realizará el envio de correo, en caso contrario no**
21. **asunto:** Asunto del correo electrónico



**Ejemplo de uso Ruby(Net Http)**

```
    [Ruby]
    require 'uri'
    require 'net/http'
    
    url = URI("https://us-central1-sri-volante.cloudfunctions.net/registroPropiedad")
    
    http = Net::HTTP.new(url.host, url.port)
    
    request = Net::HTTP::Post.new(url)
    request["Content-Type"] = 'application/json'
    request["Cache-Control"] = 'no-cache'
    request["Postman-Token"] = 'ff9258f7-326d-4a16-9e64-e3b99b5a7e9c'
    request.body = "{\n   \"codigo\":10,\n   \"imgHeader\":\"https://sri-volante.firebaseapp.com/image-cabecera.07b9b30a9727f3d8e793.jpg\",\n   \"titulo\":\"Casa\",\n   \"ubicacion\":\"El Tesoro\",\n   \"precio\":\"1260000000\",\n   \"area\":\"398\",\n   \"habitaciones\":4,\n   \"banos\":5,\n   \"parqueaderos\":2,\n   \"administracion\":\"1118000\",\n   \"predial\":null,\n   \"areas\":[\n      \"Zonas verdes\",\n      \"Salón común\",\n      \"Ascensor\"\n   ],\n   \"caracteristicas\":[\n\n   ],\n   \"imagenes\":[\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/1/medium-00033FDF-10-99d650a13826da21e6a00c1664485bcc.jpg?1529421456\",\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/2/medium-00033FDF-11-088a77052a8a4aa2a253704fc5eb940a.jpg?1529421457\",\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/3/medium-00033FDF-12-46af84016e52e0179d8de86b5bf568ed.jpg?1529421457\",\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/4/medium-00033FDF-13-94e13ad601e1a3506c65fd867c6399e2.jpg?1529421457\",\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/5/medium-00033FDF-14-97e181703c593a5dbda4e38e28e34da7.jpg?1529421457\",\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/6/medium-00033FDF-15-43cc3ec54e4a1456bbddbe1f7dd9b270.jpg?1529421457\",\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/7/medium-00033FDF-16-59917e585233a2e6319cf1edcf708513.jpg?1529421457\",\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/8/medium-00033FDF-17-c4d9d42ac9f972b9d39220e7bb22fd0f.jpg?1529421458\",\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/9/medium-00033FDF-18-cb8d5dfe5415cf355bc33bb2fa3495ca.jpg?1529421458\",\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/10/medium-00033FDF-19-45f7b0cdb0e9fc7ad72977bad1276f4a.jpg?1529421458\",\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/11/medium-00033FDF-20-b4d2c0910832c2f8e00a2d44712d925a.jpg?1529421458\",\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/12/medium-00033FDF-21-74e580851830e067dec43924a42054fc.jpg?1529421458\",\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/13/medium-00033FDF-22-b9ed8f2ecef8ceaf880758309b74f982.jpg?1529421458\",\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/14/medium-00033FDF-23-89e114f2d7a0aad496001b5b506ce3f4.jpg?1529421459\",\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/15/medium-00033FDF-24-24e25ed3d3fcf1fdfb978a89ad1d0be7.jpg?1529421459\",\n      \"http://s3.amazonaws.com/srinmobiliario-dev/images/files/16/medium-00033FDF-9-a1696234ce2fc5fdafb5c125efe8da92.jpg?1529421459\"\n   ],\n   \"imgAsesor\":\"https://sri-volante.firebaseapp.com/assets/icons/foto-asesor.jpg\",\n   \"nombreAsesor\":\"Cristian Herrera\",\n   \"fraseAsesor\":\"SRI\",\n   \"telefonoAsesor\":\"(57) 315 494 9716\",\n   \"correoAsesor\":\"cherrera@srinmobiliario.com \",\n   \"email\":\"mesasergio@gmail.com \",\n   \"asunto\":\"Propiedades de Showroom Inmobiliario\"\n}"
    
    response = http.request(request)
    puts response.read_body
```

**Ejemplo de uso Javascript XHR**

```
    [Javascript]
    var data = JSON.stringify({
      "codigo": 10,
      "imgHeader": "https://sri-volante.firebaseapp.com/image-cabecera.07b9b30a9727f3d8e793.jpg",
      "titulo": "Casa",
      "ubicacion": "El Tesoro",
      "precio": "1260000000",
      "area": "398",
      "habitaciones": 4,
      "banos": 5,
      "parqueaderos": 2,
      "administracion": "1118000",
      "predial": null,
      "areas": [
        "Zonas verdes",
        "Salón común",
        "Ascensor"
      ],
      "caracteristicas": [],
      "imagenes": [
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/1/medium-00033FDF-10-99d650a13826da21e6a00c1664485bcc.jpg?1529421456",
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/2/medium-00033FDF-11-088a77052a8a4aa2a253704fc5eb940a.jpg?1529421457",
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/3/medium-00033FDF-12-46af84016e52e0179d8de86b5bf568ed.jpg?1529421457",
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/4/medium-00033FDF-13-94e13ad601e1a3506c65fd867c6399e2.jpg?1529421457",
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/5/medium-00033FDF-14-97e181703c593a5dbda4e38e28e34da7.jpg?1529421457",
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/6/medium-00033FDF-15-43cc3ec54e4a1456bbddbe1f7dd9b270.jpg?1529421457",
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/7/medium-00033FDF-16-59917e585233a2e6319cf1edcf708513.jpg?1529421457",
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/8/medium-00033FDF-17-c4d9d42ac9f972b9d39220e7bb22fd0f.jpg?1529421458",
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/9/medium-00033FDF-18-cb8d5dfe5415cf355bc33bb2fa3495ca.jpg?1529421458",
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/10/medium-00033FDF-19-45f7b0cdb0e9fc7ad72977bad1276f4a.jpg?1529421458",
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/11/medium-00033FDF-20-b4d2c0910832c2f8e00a2d44712d925a.jpg?1529421458",
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/12/medium-00033FDF-21-74e580851830e067dec43924a42054fc.jpg?1529421458",
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/13/medium-00033FDF-22-b9ed8f2ecef8ceaf880758309b74f982.jpg?1529421458",
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/14/medium-00033FDF-23-89e114f2d7a0aad496001b5b506ce3f4.jpg?1529421459",
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/15/medium-00033FDF-24-24e25ed3d3fcf1fdfb978a89ad1d0be7.jpg?1529421459",
        "http://s3.amazonaws.com/srinmobiliario-dev/images/files/16/medium-00033FDF-9-a1696234ce2fc5fdafb5c125efe8da92.jpg?1529421459"
      ],
      "imgAsesor": "https://sri-volante.firebaseapp.com/assets/icons/foto-asesor.jpg",
      "nombreAsesor": "Cristian Herrera",
      "fraseAsesor": "SRI",
      "telefonoAsesor": "(57) 315 494 9716",
      "correoAsesor": "cherrera@srinmobiliario.com ",
      "email": "mesasergio@gmail.com ",
      "asunto": "Propiedades de Showroom Inmobiliario"
    });
    
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    
    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        console.log(this.responseText);
      }
    });
    
    xhr.open("POST", "https://us-central1-sri-volante.cloudfunctions.net/registroPropiedad");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Cache-Control", "no-cache");
    xhr.setRequestHeader("Postman-Token", "2408449e-6510-4b0a-b51b-2ca592c57aa7");
    
    xhr.send(data);
```


### propiedad

**URI del servicio GET:** [https://us-central1-sri-volante.cloudfunctions.net/propiedad](URL "https://us-central1-sri-volante.cloudfunctions.net/propiedad").

Este servicio recibe un parametro codigo con el valor del codigo MLS de la propiedad y devuelve un html renderizado para la visualización de la información guardada.

** Ejemplo de llamado **
Para invocar esta función solo se debe llamar la URI con el parametro codigo al final y enviar el codigo de la propiedad MLS

`https://us-central1-sri-volante.cloudfunctions.net/propiedad?codigo=10`



## Funciones utilitarias

1. imprimir

La función imprimir recibe 2 atributos (info, request)

* info: Es el objeto con la información de la propiedad obtenida desde la base de datos
* request: Es el objeto request del llamado de la función **Propiedad**


2. enviarCorreo

La función enviarCorreo recibe 3 atributos (email, asunto, mensaje)

* email: Correo electrónico del destinatario del volante
* asunto: Asunto del correo electrónico a enviar
* mensaje: Es el html del render de la pagina volante en email html

3. guardarDatosPropiedad

La función enviarCorreo recibe 1 atributo (info)

* info: Es el objeto que se guardará en la colección propiedades de firestore para luego ser recuperada
 




